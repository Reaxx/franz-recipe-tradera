module.exports = (Franz) =>
{
	const getMessages = function getMessages()
	{
		//Gets all items
		let items = document.querySelectorAll(".myt-list-item");

		//count - All items, iCount - importent items
		let count = 0, iCount = 0;

		items.forEach( element => {
			//Not very robust, but only hits elements with red time. Guessing that its less then 12h			
			let endingSoon = element.querySelector(".myt-list-item-content-time-ending") != null;
			let biddingStatus = element.querySelector("figcaption").innerText;

			if(biddingStatus == "ÖVERBJUDEN")
			{
				iCount++
			}			
			if(endingSoon && biddingStatus != "LEDER")
			{
				count++;
			}
		});

		Franz.setBadge(iCount,count);
		console.log("Count: "+iCount+"/"+count);

	};

	function reload()
	{
		console.log("Reloading page");
		location.reload()
		//Resetting the badge as it seems to get "stuck"
		Franz.setBadge(0);
	}

	Franz.loop(getMessages);

	//Reloads page once every minute
	window.setTimeout(reload,60000);
};
